"""
Name of the project : Python Problems
File name : python_5.py
Description : program to print the factorial of given number.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_5.py
Input : 5
Output : 120
"""


def fact(num) :
    result,i = 1 , 1
    while i<=num :
        result *= i
        i+=1
    return result
numb = input("enter the number  : ")
print( "" )
print(fact(int(numb)))
