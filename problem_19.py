"""
Name of the project : Python Problems
File name : python_19.py
Description :  Use a list comprehension to square each odd number in a list
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_19.py
Input : 1,2,3,4,5
Output : 1,9,25
"""


user_input = input("enter the numbers  :  ")
input_list = user_input.split(",")
for i in input_list :
    if int(i)%2 != 0 :
        print(int(i)*int(i),end=",")
print()
