"""
Name of the project : Python Problems
File name : python_35.py
Description : print objet with keys 1-20 and values square of the keys
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_35.py
"""


def solve() :
    obj = {}
    for i in range(1,21) :
        obj[i] = i**2
    print(obj)
solve()
