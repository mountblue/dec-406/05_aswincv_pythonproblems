"""
Name of the project : Python Problems
File name : python_32.py
Description : compare two strings and print the longest
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_32.py
Input : hello hai
Output : hello
"""


def solve(str1,str2) :
    if len(str1) > len(str2) :
        print(str1,"\n")
    elif len(str2) > len(str1) :
        print(str2,"\n")
    else :
        print(str1)
        print(str2)
input_1 = input("enter the first string  :  ")
input_2 = input("enter the second string  :  ")
solve(input_1,input_2)
