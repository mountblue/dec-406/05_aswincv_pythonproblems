"""
Name of the project : Python Problems
File name : python_23.py
Description : finds the digit up to a limit divisible by 7
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_23.py
Input : 100
Output : 0 7 14 21 28 35 42 49 56 63 70 77 84 91 98
"""


class user_details:
    def __init__(self):
        self.num = int(self.getString())
    def getString(self):
        return(input("enter the number :"))
    def solve(self) :
        for i in range(0,self.num) :
            if i%7 == 0 :
                print(i,end=" ")
obj = user_details()
print()
obj.solve()
print()
