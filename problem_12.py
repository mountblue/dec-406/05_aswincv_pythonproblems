"""
Name of the project : Python Problems
File name : python_12.py
Description : program that accepts a comma separated sequence and print
              them in sorted order.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_12.py
Input : a b c
Output : A B C
"""


lines = []
print("enter the sequence of lines   :")
while True:
    line = input()
    if line:
        lines.append(line)
    else:
        break
for i in lines :
    print(i.upper())
    print()
