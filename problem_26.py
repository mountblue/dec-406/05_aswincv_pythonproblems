"""
Name of the project : Python Problems
File name : python_26.py
Description : find the square of a given number
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_26.py
Input : 2
Output : 4
"""


def find_square(num) :
    return num**2
user_input = input("enter the number  :  ")
print(find_square(int(user_input)))
