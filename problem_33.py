"""
Name of the project : Python Problems
File name : python_33.py
Description : checks whether a given number is odd or even
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_33.py
Input : 5
Output : odd
"""


def check(num) :
    if num%2 == 0 :
        print("even\n")
    else :
        print("odd\n")
user_input = input("enter the number  :  ")
check(int(user_input))
