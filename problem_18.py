"""
Name of the project : Python Problems
File name : python_18.py
Description : program that computes the value of a+aa+aaa+aaaa with a given
              digit as the value of a.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_18.py
Input : 2
Output : 2468
"""


user_input = input("enter the number  :  ")
num1 = int(user_input)
num2 = int(user_input + user_input)
num3 = int(user_input+user_input+user_input)
num4 = int(user_input+user_input+user_input+user_input)
print("result  =  ",num1+num2+num3+num4)
print()
