"""
Name of the project : Python Problems
File name : python_27.py
Description : prints the documentation of some built in functions
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_27.py
"""


print(abs.__doc__,"\n\n\n",int.__doc__,"\n")
