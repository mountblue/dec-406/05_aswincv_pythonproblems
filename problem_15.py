"""
Name of the project : Python Problems
File name : python_15.py
Description : program will find all such numbers between 1000 and 3000 (both
              included) such that each digit of the number is an even number.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_15.py
Output : 2000,2002,...
"""


for i in range(2000,3001) :
    temp = i
    while(temp) :
        if((temp%10)%2 == 0) :
            temp = int(temp/10)
        else :
            break
    if(temp == 0) :
        print(i,end=",")
print()
