"""
Name of the project : Python Problems
File name : python_34.py
Description : print the elements of object using keys
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_34.py
Output : 1,4,9
"""


def printing(obj) :
    print(obj[1])
    print(obj[2])
    print(obj[3])
obj1 = {1 : 1,2 : 4,3 : 9}
printing(obj1)
