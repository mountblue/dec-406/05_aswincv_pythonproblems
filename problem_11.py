"""
Name of the project : Python Problems
File name : python_11.py
Description : program that accepts a comma separated sequence and print
              them in sorted order.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_11.py
Input : a,z,b
Output : a,b,z
"""


user_input = input("enter the string  :  ")
print(user_input)
str = user_input.split(",")
str.sort()
for i in range(0,len(str)) :
   print(str[i],end=",")
print()
