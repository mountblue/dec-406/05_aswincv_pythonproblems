"""
Name of the project : Python Problems
File name : python_17.py
Description : accepts a sentence and calculate the number of uppercase letters
              and lowercase letters
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_17.py
Input : Hello world!
Output : 1,9
"""


user_input = input("enter the string  :  ")
upper = 0
lower = 0
for i in user_input :
    if i.isupper() :
        upper += 1
    elif i.islower():
        lower += 1
print("number of uppercase letters  :  ",upper)
print("number of lowercase letters  :  ",lower)
print
