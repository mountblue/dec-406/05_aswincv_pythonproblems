"""
Name of the project : Python Problems
File name : python_21.py
Description : checks the validity of a password
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_21.py
Input : Hello007#
Output : valid password
"""


pass_word = input("enter the password ")
flag = [0,0,0,0]
if(len(pass_word)>12 or len(pass_word)<8) :
    print("invalid password")
    print()
else :
    for i in range(0,len(pass_word)) :
        if pass_word[i].isupper() :
            flag[0] = 1
        elif pass_word[i].islower() :
            flag[1] = 1
        elif pass_word[i].isdigit() :
            flag[2] = 1
        elif pass_word[i]=="@" or pass_word[i]=="#" or pass_word[i]=="$" :
            flag[3] = 1
    check = 0
    for i in flag :
        if i==0 :
            print("invalid password")
            print()
            check = 1
            break;
    if check == 0 :
        print("valid password")
