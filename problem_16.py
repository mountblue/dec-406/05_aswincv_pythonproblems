"""
Name of the project : Python Problems
File name : python_16.py
Description : accepts a sentence and calculate the number of letters and digits.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_16.py
Input : hello world! 123
Output : 10 ,3
"""


user_input = input("enter the string  :  ")
letter = 0
num = 0
for i in user_input :
    if i.isalpha() :
        letter += 1
    elif i.isdigit():
        num += 1
print("number of letters  :  ",letter)
print("number of digits  :  ",num)
print
