"""
Name of the project : Python Problems
File name : python_13.py
Description : accepts a sequence of whitespace separated words as input and
              prints the words after removing all duplicate words and sorting
              them alphanumerically.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_13.py
Input : hello i am your friend and i am from kerala
Output : am and friend from hello i kerala your
"""


user_input = input("enter the string   :  ")
str = user_input.split(" ")
str = list(set(str))
str.sort()
for i in str :
    print(i,end= " ")
print()
