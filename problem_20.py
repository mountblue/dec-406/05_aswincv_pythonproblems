"""
Name of the project : Python Problems
File name : python_20.py
Description :  to do the bank transaction and finally calculate the total amount
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_20.py
Input : D 300 D 300 W 200 D 100
Output : 500
"""


user_input = []
print("enter the transactions  :  ")
print()
tot = 0
while 1 :
    temp = input()
    if temp :
        user_input.append(temp)
    else :
        break;
for i in user_input :
    if i[0] == "D" :
        tot+=int(i[2:])
    else :
        tot-=int(i[2:])
print("remaining amount  =  ",tot)
print()
