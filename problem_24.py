"""
Name of the project : Python Problems
File name : python_24.py
Description : distance between 2 points
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_24.py
Input : UP 5 DOWN 3 LEFT 3 RIGHT 2
Output : 2
"""


import math
pos_x = 0
pos_y = 0
user_input = []
print("enter the movement   :   \n")
while 1 :
    temp = input()
    if temp :
        user_input.append(temp)
    else :
        break
temp = []
for i in user_input :
    temp = i.split(" ")
    if temp[0] == "UP" :
        pos_y += int(temp[1])
    elif temp[0] == "DOWN" :
        pos_y -= int(temp[1])
    elif temp[0] == "LEFT" :
        pos_x -= int(temp[1])
    else :
        pos_x += int(temp[1])
print("positions after movement :  x=",pos_x,"  y=",pos_y,"\n")
print("distance = ",int(math.sqrt(pos_x**2+pos_y**2)),"\n")
