"""
Name of the project : Python Problems
File name : python_14.py
Description : accepts a sequence of comma separated 4 digit binary numbers as
              its input and then check whether they are divisible by 5 or not.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  python_14.py
Input :  0100,0011,1010,1011
Output : 1010
"""


user_input = input("enter the numbers  :  ")
num_list = user_input.split(",")
dec_list = []
for i in num_list :
    dec_list.append(int(i,2))
for i in range(0,len(dec_list)) :
    if dec_list[i]%5 == 0 :
        print(num_list[i],end=",")
print()
